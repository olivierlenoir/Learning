// Author: Olivier Lenoir <olivier.len02@gmail.com>
// Created: 2024-03-03
// Language: Rust 1.76
// Project: Hello World

fn main() {
    println!("Hello World!");
    println!("I'm a Rustacean!");
    println!("It's {} today.", "rainning");
    println!("My name is {name}.", name="Olivier");
    println!("I'm {}.", 51);
}
